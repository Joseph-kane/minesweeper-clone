﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    //-----------Variables----------//
    private GridElementScript gridElementScript;
    private WinManager winManager;
    private SpriteRenderer spriteRenderer;
    private CellSprites cellSprites; //Holds refrences to all sprite files
    private Populate populateScript;

    

    //-----------Methods-----------//

    void Start ()
    {
        gridElementScript = GetComponent<GridElementScript>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        winManager = Camera.main.GetComponent<WinManager>();
        cellSprites = Camera.main.GetComponent<CellSprites>();
        populateScript = Camera.main.GetComponent<Populate>();
    }
	
	void OnMouseDown() //When the cell is clicked
    {
        if(!winManager.firstClickDone) //If this click is the first
        {
            winManager.firstClickDone = true;
            winManager.gameInProgress = true; //Mark game as started

            //The first click in minesweeper must never be a mine
            while (gridElementScript.IsMine) //Loops through placing a mine until this spot isnt a mine
            {
                gridElementScript.IsMine = false;
                populateScript.PlaceRandomMine(); //Place a new mine randomly
            }
            
            //will always be empty cell (no need for check)
            HandleCell(gridElementScript.XCoord, gridElementScript.YCoord); //Reveal empty cell and empty area around it

            winManager.CheckForWin();
        }
        else if (!gridElementScript.IsRevealed && !gridElementScript.IsFlagged && winManager.gameInProgress) //If the cell hasnt already been revealed && isnt tagged with a flag && game is still in progress
        {
            if (gridElementScript.IsMine)
            {
                gridElementScript.IsRevealed = true; //Mark as revealed
                spriteRenderer.sprite = cellSprites.mineSprite;
                winManager.Lose(); // Lose function
            }
            else
            {
                HandleCell(gridElementScript.XCoord, gridElementScript.YCoord); //Reveal empty cell and empty area around it
            }

            winManager.CheckForWin();
        }
    }

    void OnMouseOver()
    {
        if(Input.GetMouseButtonDown(1) && !gridElementScript.IsRevealed && winManager.gameInProgress) //Right clicking for marking a cell with a flag
        {
            spriteRenderer.sprite = gridElementScript.IsFlagged ? cellSprites.hiddenTileSprite : cellSprites.flagTileSprite;
            gridElementScript.IsFlagged = !gridElementScript.IsFlagged; //Invert the flagged boolean
        }
    }

    /// <summary>
    /// Handles revealing of a non mine cell and the empty area around it if it is an empty cell
    /// </summary>
    /// <param name="x">X position of the cell to reveal</param>
    /// <param name="y">Y position of the cell to reveal</param>
    private void HandleCell(int x, int y)
    {
        if (x >= 0 && x < Populate.mineField.GetLength(0) && y >= 0 && y < Populate.mineField.GetLength(1)) //Only run when in bounds of minefield 
        {
            GameObject curCell = Populate.mineField[x, y]; //Creat reference to the gameobject of the cell at the inputted x an y
            GridElementScript curCellGridScript = curCell.GetComponent<GridElementScript>();

            if (!curCellGridScript.IsRevealed)
            {
                curCellGridScript.IsRevealed = true; //Mark as clicked and revealed
                SpriteRenderer curCellSpriteRenderer = curCell.GetComponent<SpriteRenderer>();
                winManager.revealedCells++; //Increase win managers revealed cells count

                int surroundingMines = Populate.GetNumSurroundingMines(x, y);

                if (surroundingMines > 0) //When there is a mine in a surrounding cell
                {
                    curCellSpriteRenderer.sprite = cellSprites.numberedSprites[surroundingMines - 1]; //Set the sprite to the number of surrounding mines
                }
                else //When there is no mines in surrounding cells
                {
                    curCellSpriteRenderer.sprite = cellSprites.emptyTileSprite; //Set sprite for this empty cell

                    //Process surrounding cells
                    HandleCell(x    , y + 1); //North
                    HandleCell(x    , y - 1); //South
                    HandleCell(x + 1, y    ); //East
                    HandleCell(x - 1, y    ); //West
                }
            }
        }
    }

 
}
