﻿//Stores refrences of all cell sprites in one location


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellSprites : MonoBehaviour
{
    //Sprites
    [Tooltip("The sprite for a tile that hasnt been clicked yet")]
    public Sprite hiddenTileSprite;

    [Tooltip("The sprite for an empty tile")]
    public Sprite emptyTileSprite;

    [Tooltip("The sprite for a tile containing a mine")]
    public Sprite mineSprite;

    [Tooltip("The sprite for marking a tile with a flag")]
    public Sprite flagTileSprite;

    [Tooltip("Numbered sprites in order from 1 to 8")]
    public Sprite[] numberedSprites;
}
