﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ReturnToMenu : MonoBehaviour
{
	public void ReturnToMenuButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}
}
