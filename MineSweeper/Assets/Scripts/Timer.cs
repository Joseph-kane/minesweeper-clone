﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    private float timer;
    public Text timerText;
    public WinManager winManagerScript;

    void Start ()
	{
        if (timerText == null)
            GetComponent<Text>();

        timer = 0;
	}

    public void ResetTimer()
    {
        timer = 0;
    }

	void Update ()
	{
        if (winManagerScript.gameInProgress)
        {
            timer += Time.deltaTime;

            //Update UI timer
            timerText.text = string.Format("{0}:{1:00}:{2:0}", Mathf.Floor(timer / 60), Mathf.Floor(timer % 60), Mathf.Floor((timer % 1) * 10));
        }

    }
}
