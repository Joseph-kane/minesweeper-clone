﻿using UnityEngine;

public class RestartGame : MonoBehaviour
{
    public WinManager winManagerScript;
    public Timer timerScript;
    public Populate populateScript;

    //Restarts the game with the same mine size and grid size
    public void Restart()
    {
        winManagerScript.SetVariablesToDefaults();
        timerScript.ResetTimer();
        foreach (GameObject cell in Populate.mineField)
        {
            Destroy(cell);
        }
        

        populateScript.CreateGrid();
    }
}