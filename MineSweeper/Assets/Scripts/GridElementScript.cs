﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridElementScript : MonoBehaviour
{
    //-----------Variables----------//

    private bool isMine; //Is this tile a mine
    private int xCoord;
    private int yCoord;
    private bool isRevealed; //Has the tile been revealed or is it still hidden
    private bool isFlagged; //Has the tile has been marked with a flag



    //-----------Properties---------//

    public bool IsMine 
    {
        set { isMine = value; }
        get { return isMine; }
    }

    public int XCoord
    {
        get { return xCoord; }
        set { xCoord = value; }
    }

    public int YCoord
    {
        get { return yCoord; }
        set { yCoord = value; }
    }

    public bool IsRevealed
    {
        get { return isRevealed; }
        set { isRevealed = value; }
    }

    public bool IsFlagged
    {
        get { return isFlagged; }
        set { isFlagged = value; }
    }



    //-----------Methods----------//

    void Start ()
    {
        isRevealed = false;
        isFlagged = false;
	}
}
