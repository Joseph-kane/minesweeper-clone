﻿//Class for methods relating to winning or losing the game

using UnityEngine;
using UnityEngine.UI;

public class WinManager : MonoBehaviour
{
    [HideInInspector]
    public int revealedCells; // Counter for the amount of minefield cells that have been revealed
    [HideInInspector]
    public bool gameInProgress; // True if the game is in progress or false if it has ended
    [HideInInspector]
    public bool firstClickDone; //Has the first click been made yet

    private Populate populateScript;
    private CellSprites cellSprites;
    private CameraShake cameraShakeScript;

    public GameObject confettiParticleSystem;
    public Text winText;
    public Text loseText;

    void Start ()
	{
        SetVariablesToDefaults();

        populateScript = Camera.main.GetComponent<Populate>();
        cellSprites = Camera.main.GetComponent<CellSprites>();
        cameraShakeScript = GetComponent<CameraShake>();
    }

    /// <summary>
    /// Sets variables back to original values
    /// </summary>
    public void SetVariablesToDefaults()
    {
        revealedCells = 0;
        gameInProgress = false;
        firstClickDone = false;
    }


    /// <summary>
    /// Checks if the game has been won
    /// </summary>
    public void CheckForWin()
    {
        if (revealedCells >= (populateScript.gridSizeX * populateScript.gridSizeY) - populateScript.totalMines)
            Win();
    }

    public void Win()
    {
        ShowAllMines();
        winText.enabled = true;
        Confetti();
        gameInProgress = false;
        Debug.Log("Win");
    }

    public void Lose()
    {
        ShowAllMines();
        cameraShakeScript.StartShake();
        gameInProgress = false;
        loseText.enabled = true;
    }

    /// <summary>
    /// Reveals all mines in the grid
    /// </summary>
    private void ShowAllMines()
    {
        for (int y = 0; y < populateScript.gridSizeY; y++)
        {
            for (int x = 0; x < populateScript.gridSizeX; x++)
            {
                GameObject cell = Populate.mineField[x, y]; 
                //GridElementScript cellGridElementScript = cell.GetComponent<GridElementScript>();
                //SpriteRenderer cellSpriterenderer = cell.GetComponent<SpriteRenderer>();

                if (cell.GetComponent<GridElementScript>().IsMine)
                {
                    cell.GetComponent<SpriteRenderer>().sprite = cellSprites.mineSprite;
                }
            }
        }
    }

    /// <summary>
    /// Starts a confetti effect
    /// </summary>
    private void Confetti()
    {
        float confettiScale = 0.04f * Camera.main.orthographicSize;
        confettiParticleSystem.transform.localScale = new Vector3(confettiScale, confettiScale, confettiScale);
        ParticleSystem.MainModule main = confettiParticleSystem.GetComponent<ParticleSystem>().main;
        main.gravityModifierMultiplier = 0.16f * Camera.main.orthographicSize;

        confettiParticleSystem.transform.position = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, -0.1f, 1)); //Set particle system position to below bottom of screen
        confettiParticleSystem.SetActive(true); //Turn on particle system
    }
}
