﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuIncorrectInputChecks : MonoBehaviour
{
    [Tooltip("Text for displaying warning of incorrect inputs")]
    public Text InputWarningText;

    [Tooltip("Input field for total mines")]
    public InputField totalMineInputField;
    [Tooltip("Input field for size of grid on the X axis")]
    public InputField xSizeInputField;
    [Tooltip("Input field for size of grid on the Y axis")]
    public InputField ySizeInputField;

    [Tooltip("Button to start the game")]
    public Button startButton;

    /// <summary>
    /// Checks if the input on the main menu for game settings are allowed, and stops the player from starting the game when it is not
    /// </summary>
    public void checkValidMenuInput()
    {
        int totalMineInputValue, xSizeInputValue, ySizeInputValue;

        if (
            int.TryParse(totalMineInputField.text, out totalMineInputValue) &&
            int.TryParse(xSizeInputField.text, out xSizeInputValue) &&
            int.TryParse(ySizeInputField.text, out ySizeInputValue)
           )
        {
            if (totalMineInputValue >= xSizeInputValue * ySizeInputValue) //If there will be more mines than tiles or same amout of mines as tiles
            {
                invalidInput("Warning: Too many mines for Grid Size");
            }
            else if (totalMineInputValue < 1)
            {
                invalidInput("There must be atleast 1 mine");
            }
            else if (xSizeInputValue <= 1 || ySizeInputValue <= 1) //If the grid X or Y size will be 1 tile or less
            {
                invalidInput("Grid sizes cannot be 1 tile or less");
            }
            else if (xSizeInputValue > 100 || ySizeInputValue > 100) //Limit grid sizes to 100 to avoid stack overflows on the mine checking recursion
            {
                invalidInput("Grid size limit of 100");
            }
            else //Input is valid
            {
                validInput();
            }
        }
        else
        {
            invalidInput("Invalid input fields");
        }
        
    }

    private void invalidInput(string warningMessage)
    {
        startButton.interactable = false;
        InputWarningText.enabled = true;
        InputWarningText.text = warningMessage;
    }

    private void validInput()
    {
        startButton.interactable = true;
        InputWarningText.enabled = false;
    }
}
