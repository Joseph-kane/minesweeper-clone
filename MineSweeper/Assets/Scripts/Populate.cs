﻿using UnityEngine;
using UnityEngine.UI;

public class Populate : MonoBehaviour
{
    //-----------Variables----------//

    [Tooltip("The amount of mines to randomly place on the minefield")]
    public int totalMines;
    [Tooltip("The size on the X axis of the grid")]
    public int gridSizeX;
    [Tooltip("The size on the Y axis of the grid")]
    public int gridSizeY;

    [Tooltip("The prefab for grid elements")]
    public GameObject gridElementPrefab;

    [Tooltip("Input field for total mines")]
    public InputField totalMinesInputField;
    [Tooltip("The input field for the X grid size")]
    public InputField xInputField;
    [Tooltip("The input field for the Y grid size")]
    public InputField yInputField;

    public static GameObject[,] mineField;



    //-----------Methods-----------//

        
    /// <summary>
    /// Setup inputted grid value and starts game
    /// </summary>
    public void InitialiseGame()
    {
        //Get Grid X and Y sizes from input field
        gridSizeX = int.Parse(xInputField.text);
        gridSizeY = int.Parse(yInputField.text);

        //Get total mines from input field
        totalMines = int.Parse(totalMinesInputField.text);

        CreateGrid();
    }

    /// <summary>
    /// Creates the games minefield and starts gameplay
    /// </summary>
    public void CreateGrid()
    {
        mineField = new GameObject[(int)gridSizeX, (int)gridSizeY];

        //Fills the grid with tiles
        for (int y=0; y < gridSizeY; y++)
        {
            for (int x=0; x < gridSizeX; x++)
            {
                mineField[x, y] = Instantiate(gridElementPrefab, new Vector2(x, y), Quaternion.identity);
                mineField[x, y].GetComponent<GridElementScript>().XCoord = x;
                mineField[x, y].GetComponent<GridElementScript>().YCoord = y;
            }
        }

        //Place mines
        for (int i = 0; i < totalMines; i++)
        {
            PlaceRandomMine();
        }

        //Camera Alignment
        transform.position = new Vector3((gridSizeX/2f)-0.5f, (gridSizeY/2f)-0.5f, -10); //Align Camera to the centre of the grid      
        GetComponent<Camera>().orthographicSize = Mathf.Max(gridSizeX/Camera.main.aspect, gridSizeY) / 2f; //Scale the Camera to fit the grid
    }

    /// <summary>
    /// Calculates how many mines are in tiles surround the inputed tile
    /// </summary>
    /// <param name="xCoord">X coordinate on the mine field grid of the tile</param>
    /// <param name="yCoord">Y coordinate on the mine field grid of the tile</param>
    /// <returns>An int between 1-8 representing the amount of mines in surrounding tiles</returns>
    public static int GetNumSurroundingMines(int xCoord, int yCoord)
    {
        int total = 0;

        //Loop through all the surrounding tiles by adding an offset in a range of -1 to +1 for the x and y values and
        //      increase the total counter variable every time one of these surrounding tiles contains a mine
        for (int yOffset = -1; yOffset <= 1; yOffset++)
        {
            if (yCoord+yOffset >= 0 && yCoord+yOffset < mineField.GetLength(1)) //If the yOffset coordinate will be inside the grid
            {
                for (int xOffset = -1; xOffset <= 1; xOffset++)
                {
                    if (xCoord+xOffset >= 0 && xCoord+xOffset < mineField.GetLength(0)) //If the xOffset coordinate will be inside the grid
                    {
                        if (!(xOffset == 0 && yOffset == 0) /*Dont count current tile*/ && IsCellMine(xCoord + xOffset, yCoord + yOffset)) //Is it a mine
                            total++;
                    }
                }
            }
        }

        return total;
    }

    /// <summary>
    /// Helper method for checking if the given cell contains a mine
    /// </summary>
    /// <param name="x">The X coordinate on the grid of the cell to check</param>
    /// <param name="y">The Y coordinate on the grid of the cell to check</param>
    /// <returns></returns>
    private static bool IsCellMine(int x, int y)
    {
        return mineField[x, y].GetComponent<GridElementScript>().IsMine ;
    }

    /// <summary>
    /// Randomly places a mine
    /// </summary>
    public void PlaceRandomMine()
    {
        int x, y; //Positions to place the mine

        //Randomly assign x and y values until that location doesnt already have a mine
        do
        {
            x = Random.Range(0, mineField.GetLength(0));
            y = Random.Range(0, mineField.GetLength(1));
        }while (IsCellMine(x, y));

        mineField[x, y].GetComponent<GridElementScript>().IsMine = true;
    }


}
