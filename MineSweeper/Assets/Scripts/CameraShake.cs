﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CameraShake : MonoBehaviour
{
    public Transform camTransform;

    [Tooltip("The amount of time that shakes will last")]
    public float shakeDuration = 0.15f;
    public float shakeAmount = 0.2f;
    //The amount of time left on the current shake
    private float shakeDurationRemaining = 0f;

    private Vector3 originalPos;

    void Awake()
    {
        if (camTransform == null)
            camTransform = Camera.main.GetComponent<Transform>();
    }

    public void StartShake()
    {
        StartCoroutine(Shake());
    }

    IEnumerator Shake()
    {
        originalPos = camTransform.localPosition;
        shakeDurationRemaining = shakeDuration;

        while (shakeDurationRemaining > 0f)
        {
            camTransform.position = originalPos + (Vector3)Random.insideUnitCircle * shakeAmount;
            //camTransform.localPosition = originalPos + (Vector3)Random.insideUnitSphere * shakeAmount;

            shakeDurationRemaining -= Time.deltaTime;
            yield return null; //comes back next frame
        }

        shakeDurationRemaining = 0f;
        camTransform.position = originalPos;
    }
}